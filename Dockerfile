FROM node:7.10.0

ARG PORT
ARG SSL_PORT
ARG HOST
ARG SSL
ARG CSP

ENV APP_NAME "frob-ng"
ENV APP_USER "frob"
ENV HOME /home/$APP_USER
ENV APP_DIR $HOME/$APP_NAME

RUN useradd --user-group --create-home --shell /bin/false $APP_USER
RUN npm install -g bower snazzy

WORKDIR $APP_DIR

COPY package.json $APP_DIR/package.json
COPY bower.json $APP_DIR/bower.json

RUN yarn install && bower install --allow-root

COPY . $APP_DIR

RUN chown -R $APP_USER:$APP_USER $HOME/*

USER $APP_USER
WORKDIR $APP_DIR

EXPOSE 8080

RUN make build
RUN make watch

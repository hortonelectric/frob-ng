# make sure you already followed instructions for at least the HAPI repo, if not HAPI AND burst-node
# clone the repo first
## `tmux attach` and make a new pane for the gulp build and run:
```
yarn config set save-prefix false
npm run setup
```
### if your API server is not on localhost:2053 edit the statement below to point to your server's IP or hostname
```
make API=123.123.123.123:1234 watch
```
## If you already have proper certificate files in `./ssl` folder, called `key` and `cert`, and want to run the production server,
### `ctrl+b + c` to make a new tmux pane and run the server
```
make server
```
### go to the server IP port http://serverip
## If you want to run in dev mode
```
make dev
```
### go to http://localhost:8080
#### note that this turns off ssl and csp but this can be overriden (see Makefile for options)
#### quick note for chrome: 
If you're getting redirects to https after you turned off ssl in dev mode, you have to clear browser history back to where the redirect first occurred. Try to run with cache disabled and dev tools open all the time to prevent this too.
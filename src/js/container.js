(function (angular) {
  'use strict'

  var app = angular.module('app')

  var ContainerCtrl = function ($rootScope, $scope, $location, NAV_CONFIG) {
    var baseTitle = ''

    $scope.navbarCollapsed = true

    $scope.title = baseTitle

    $scope.$on('title', function (evt, val, replace) {
      var parts = $scope.title.split(' - ')
      if (replace) {
        parts = [baseTitle]
      } else {
        if (parts.length > 2) {
          parts.pop()
        }
      }
      if (val) {
        parts.push(val)
      }
      $scope.title = parts.join(' - ')
    })

    $scope.toggleSidenav = function (compId) {
      if (compId === undefined) {
        compId = 'mainNav'
      }
    }

    $scope.navLinks = NAV_CONFIG

    $scope.navLinks.forEach(function (link) {
      var regexp = new RegExp(link.href)
      if (regexp.test($location.path())) {
        $scope.activeLink = link
        $scope.$emit('title', link.title, true)
      }
    })

    $scope.navigate = function (link, path, state) {
      $scope.activeLink = link
      var newPath = link.prefix + path.href
      if (state) {
        if (state.href) {
          newPath += state.href
        } else if (state.conf.url) {
          newPath += state.conf.url
        }
      }
            // console.log(newPath);
      $location.path(newPath)
            // $scope.$emit('title', link.title, true);
            // $scope.$emit('title', path.title);
      $scope.navbarCollapsed = true
    }
  }

  app.controller('ContainerCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'NAV_CONFIG',
    ContainerCtrl
  ])
})(window.angular)

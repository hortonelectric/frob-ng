(function (angular) {
  'use strict'

  angular.module('app')
        .run(runBlock)

  runBlock.$inject = ['$window', '$location', '$rootScope', '$state', '$localStorage', 'Api', 'isMobile']

  function runBlock ($window, $location, $rootScope, $state, $localStorage, Api, isMobile) {
    $rootScope.currentVersion = '0.0.1'
    $rootScope.clientVersion = 'v0.0.13'
        // catch ui router errors
    $rootScope.$on('$stateChangeError', console.error.bind(console))
    $rootScope.ACCOUNT = null
    $rootScope.USER = null
    $rootScope.GAME = null
    $rootScope.SESSION = null
    $rootScope.isMobile = isMobile
    $rootScope.socketConnected = false
    $rootScope.tableFocused = false
    $rootScope.chatFocused = false
    $rootScope.fontsLoaded = false
    $rootScope.roundHistory = []
    if (document.fonts) {
      document.fonts.ready.then(function () { $rootScope.fontsLoaded = true })
    } else {
      $rootScope.fontsLoaded = true
    }

    $rootScope.$state = $state

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
 //, fromState

      $rootScope.toParams = toParams
      $rootScope.toState = toState
    })

    $rootScope.bodyClass = function () {
      if ($rootScope.$state.current.url === '/') {
        return 'game'
      } else {
        return 'content'
      }
    }

    $rootScope.focusChat = function () {
      if ($rootScope.isMobile.any()) {
        $rootScope.chatFocused = true
      }
    }

    $rootScope.focusTable = function () {
      $rootScope.tableFocused = true
    }
    $rootScope.blurTable = function () {
      $rootScope.tableFocused = false
    }

    $rootScope.blurChat = function () {
      $rootScope.chatFocused = false
                // console.log('blur chat')
                // setTimeout(function() {
                //   $rootScope.$apply(function() {
                //     document.getElementById("chat-window").scrollTop = document.getElementById("chat-window").scrollHeight;
                //   });
                // },300);
    }

    $rootScope.$on('http error', function (evt, rejection) {
      var data = rejection.data
      if (!data) {
        data = {error: rejection.statusText, message: rejection.statusText}
      }
      var err = new Error(data.message)
      err.code = rejection.status
    })

    if ($localStorage.authHeader) {
      Api.authHeader = $localStorage.authHeader
      $rootScope.USER = $localStorage.USER
      $rootScope.SESSION = $localStorage.SESSION
    }
    Api.connectSocket()

    $rootScope.selectText = function ($event) {
      var selection = $window.getSelection()
      var range = document.createRange()
      range.selectNodeContents($event.target)
      selection.removeAllRanges()
      selection.addRange(range)
    }

    $rootScope.loadingTips = [
      'Made to Burst!'
    ]

    $rootScope.isLoading = false
    $rootScope.showLoading = function (transparent) {
      if (transparent) {
        $rootScope.isLoadingTransparent = true
      } else {
        $rootScope.loadingTip = $rootScope.loadingTips[ Math.floor((Math.random() * $rootScope.loadingTips.length) + 1) - 1 ]
        $rootScope.isLoading = true
      }
    }
    $rootScope.isLandscape = false
    function checkOrientation () {
      if ($rootScope.isMobile.any() && $window.innerHeight + 150 < $window.innerWidth) {
        $rootScope.isLandscape = true
      } else {
        $rootScope.isLandscape = false

        $rootScope.$broadcast('windowResize')
      }
      $rootScope.$apply()
    }
    angular.element($window).bind('resize', checkOrientation)
    $window.addEventListener('orientationchange', checkOrientation)

    $rootScope.hideLoadingTransparent = function (resTime) {
      var time = 200
      time -= resTime || 0
      if (time > 0) {
        setTimeout(function () {
          $rootScope.$apply(function () {
            $rootScope.isLoadingTransparent = false
          })
        }, time)
      } else {
        $rootScope.isLoadingTransparent = false
      }
    }

    $rootScope.hideLoading = function (resTime) {
      var time = 2500
      time -= resTime || 0
      if (time > 0) {
        setTimeout(function () {
          $rootScope.$apply(function () {
            $rootScope.isLoading = false
          })
        }, time)
      } else {
        $rootScope.isLoading = false
      }
    }

        // window.onbeforeunload = function() {
        //     return "EXIT THE GAME?";
        // };
    checkOrientation()
  }
})(window.angular)

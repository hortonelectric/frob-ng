(function (angular) {
  'use strict'

  angular.module('app', [
    'ui.router', 'ngToast',
    'srcMapExceptionHandler', 'angular-loading-bar', 'ngStorage', 'ui.gravatar'
  ])
})(window.angular)

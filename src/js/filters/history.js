(function (window, angular) {
  'use strict'

  var app = angular.module('app')

  app.filter('profit', function () {
    return function (value, record) {
      if (!record.cashoutMultiplier && !record.finishedAt) {
        return '-'
      }
      return value
    }
  })
})(window, window.angular)

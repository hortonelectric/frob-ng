(function (window, angular) {
  'use strict'

  var app = angular.module('app')

  var padNumber = function (number, decimals, zeropad) {
    number = parseFloat(number)
    var output = number.toString()
    decimals = parseInt(decimals, 10)
    var parts
    if (!isNaN(decimals)) {
      parts = output.split('.')
      if (parts[1]) {
        parts[1] = parts[1].slice(0, decimals)
      } else {
        parts[1] = new Array(decimals + 1).join('0')
      }
      var leftover = decimals - parts[1].length
      if (leftover > 0) {
        parts[1] += new Array(leftover + 1).join('0')
      }
      output = parts.join('.')
    }
    zeropad = parseInt(zeropad, 10)
    if (!isNaN(zeropad)) {
      parts = output.split('.')
      while (parts[0].length < zeropad) {
        parts[0] = '0' + parts[0]
      }
      output = parts.join('.')
    }
    return output
  }

  var addCommas = function (input) {
    var output = input.toString()
    var parts = output.split('.')
    var principal = parts[0].replace(',', '')
    var isNegative = false
    if (principal[0] === '-') {
      isNegative = true
      principal = principal.slice(1)
    }
    var newPrincipalParts = []
    for (var i = principal.length - 1, iters = 1; i >= 0; i--, iters++) {
      newPrincipalParts.unshift(principal[i])
      if (i !== 0 && iters % 3 === 0) {
        newPrincipalParts.unshift(',')
      }
    }
    if (newPrincipalParts[0] === ',') {
      newPrincipalParts.shift()
    }
    var newPrincipal = newPrincipalParts.join('')
    if (isNegative) {
      newPrincipal = '-' + newPrincipal
    }
    parts[0] = newPrincipal
    if (!parts[1]) {
      return newPrincipal
    } else {
      return parts.join('.')
    }
  }

  var cryptoFilter = function () {
    return function (input, decimals, zeropad) {
      if (input === undefined) {
        return '-'
      } else {
        if (!input) { return '-' }
        var btc = parseInt(input, 10).toBitcoin()
        if (input < 100 && input > 0) {
          input = Math.floor(input)
          if (input < 10) {
            btc = '0.0000000' + input
          } else {
            btc = '0.000000' + input
          }
        } else {
          btc = padNumber(btc, decimals, zeropad)
        }
        return addCommas(btc)
      }
    }
  }

  angular.module('app').filter('bits', ['$filter', function ($filter) {
    var currencyFilter = $filter('number')
    return function (input) {
      return currencyFilter(input / 100, 2)
    }
  }])

  app.filter('cryptovalue', cryptoFilter)
  app.filter('paddednumber', function () {
    return function (input, decimals, zeropad) {
      return addCommas(padNumber(input, decimals, zeropad))
    }
  })

  app.filter('numberk', function () {
    return function (input) {
            // console.log(input);
      if (parseInt(input, 10) < 1000) {
        return input
      } else {
        var retval = (parseInt(input, 10) / 1000)
        retval = retval + 'K'
        return retval
      }
    }
  })
})(window, window.angular)

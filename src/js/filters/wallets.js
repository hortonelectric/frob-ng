(function (window, angular) {
  'use strict'

  var app = angular.module('app')

  app.filter('walletField', function () {
    return function (wallets, currency, key) {
      if (!wallets) { return }

      for (var i = 0; i < wallets.length; i++) {
        if (wallets[i].currency === currency) {
          return wallets[i][key]
        }
        continue
      }
      return 'Error'
    }
  })

  app.filter('withdrawAddress', function () {
    return function (wallets, currency) {
      if (!wallets) { return }

      for (var i = 0; i < wallets.length; i++) {
        if (wallets[i].currency === currency) {
          return wallets[i].address
        }
        continue
      }
      return 'Error'
    }
  })
  app.filter('balance', ['$filter', function ($filter) {
    return function (wallets, currency) {
      if (!wallets) { return }
      for (var i = 0; i < wallets.length; i++) {
        if (wallets[i].currency === currency) {
          var cryptovalue = $filter('cryptovalue')
          return cryptovalue(wallets[i].balance)
        }
        continue
      }
      return 'Error'
    }
  }])
})(window, window.angular)

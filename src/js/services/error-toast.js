(function (angular) {
  'use strict'

  var app = angular.module('app')

  var ErrorToastFactory = function ($rootScope, $sce, ngToast) {
    var scope = $rootScope.$new()
    var ErrorToast = {}

    scope.hide = function () {
    }

    ErrorToast.error = function (err) {
      var errScope = $rootScope.$new()
      if (typeof err === 'string') {
        err = {message: err}
      }
      errScope.err = err
      ngToast.danger({
        compileContent: errScope,
        content: $sce.trustAsHtml(
                                          '<span ng-if="err.code">[{{err.code}}] -</span> {{err.message}}'),
        dismissOnTimeout: true,
        timeout: 6000,
        dismissButton: true
      })
    }

    ErrorToast.errorHandler = function (res, status) {
      var err
      if (!status) {
        status = res.status || res.code || 500
      }
      if (res.data) {
        res = res.data
        res.status = status
      }
      if (typeof res === 'string') {
        err = res
      } else {
        err = '[' + status + '] ' + res.message
      }
      ErrorToast.error(err)
    }

    ErrorToast.show = function (message) {
      ngToast.create(message)
    }

    return ErrorToast
  }

  app.factory('Toast', [
    '$rootScope',
    '$sce',
    'ngToast',
    ErrorToastFactory
  ])
})(window.angular)

/* global io, Fingerprint2 */
(function (angular) {
  'use strict'

  var app = angular.module('app')

  app.factory('Api', Api)
  Api.$inject = ['$rootScope', 'Toast']
  function Api ($rootScope, Toast) {
    var Api = {
      protocol: 'http://',
      host: '<%= api.host %>',
      basePath: '/api/',
      authHeader: null
    }

    Object.defineProperty(Api, 'authHeader', {
      get: function () {
        return this._authHeader
      },
      set: function (authHeader) {
        this._authHeader = authHeader
        return authHeader
      }
    })

    Api.url = function (path) {
      if (path === undefined) {
        path = ''
      } else if (path[0] !== '/') {
        path = '/' + path
      }
      return this.protocol + this.host + this.basePath + path
    }

    Api.rootUrl = function (path) {
      if (path === undefined) {
        path = ''
      } else if (path[0] !== '/') {
        path = '/' + path
      }
      return this.protocol + this.host + path
    }

    new Fingerprint2().get(function (result, components) {
      Api.fingerprint = {hash: result, components: components}
    })

    Api.connectSocket = function () {
      if (Api.socket) {
        $rootScope.socketConnected = false
        Api.socket.disconnect()
        Api.socket = undefined
      }
            // some would say this is 'not secure',
            // but as long as it uses WSS:// then the
            // URL and query string are encrypted and it IS secure
            // unless the user's browser is compromised.
      Api.socket = io('http://' + Api.host + '?authHeader=' + Api.authHeader, {secure: true, forceNew: true})
      Api.socket.on('connect', function () {
        $rootScope.socketConnected = true

                // let the server know that this socket connected
        Api.socket.emit('game')
        Api.socket.emit('initial-rounds')
        Api.socket.emit('initial-bets')
      })

            // this one gets fired every 250ms
      Api.socket.on('game-info', function (data) {
        $rootScope.GAME = data.game
        $rootScope.PLAYERS = data.players // contains bet info
        $rootScope.serverTime = data.serverTime
        $rootScope.$broadcast('game-info', data)
      })
            // end or begin a game
      Api.socket.on('multiple-sockets', function () {
        $rootScope.multipleSockets = true
      })
            // end or begin a game
      Api.socket.on('game-bust', function (data) {
        $rootScope.$broadcast('game-bust', data)
      })
      Api.socket.on('cashout-all', function () {
        $rootScope.cashoutAll = true
      })
      Api.socket.on('game-start', function (data) {
        $rootScope.$broadcast('game-start', data)
      })

      Api.socket.on('initial-rounds', function (data) {
        $rootScope.roundHistory = data.data
        $rootScope.$broadcast('initial-rounds', data)
      })
            /* jshint ignore:start */
      Api.socket.on('initial-bets', function (data) {
        for (var i = 0; i < $rootScope.roundHistory.length; i++) {
          var me = _.find(data.data, function (p) {
            return p.roundId === $rootScope.roundHistory[i]._id.toString()
          })
          _.assign($rootScope.roundHistory[i], me)
        }
        $rootScope.$broadcast('initial-bets', data)
      })
      Api.socket.on('game-history', function (data) {
        var me = _.find(data.players, function (p) {
          return $rootScope.ACCOUNT && p.accountId === $rootScope.ACCOUNT._id
        }
                )
        _.assign(data.round, me)
        $rootScope.roundHistory.unshift(data.round)
        $rootScope.roundHistory.pop()
        $rootScope.$broadcast('game-history', data)
      })

            /* jshint ignore:end */

            // in-between games countdown
      Api.socket.on('game-countdown', function (data) {
        $rootScope.$broadcast('game-countdown', data)
      })
      Api.socket.on('game-countdown-start', function () {
        $rootScope.cashoutAll = false
        $rootScope.$broadcast('game-countdown-start')
      })
            // every 250ms during an active game
      Api.socket.on('game-heartbeat', function (data) {
        $rootScope.$broadcast('game-heartbeat', data)
      })

            // for individual players
      Api.socket.on('user-update', function (data) {
        $rootScope.USER = data
        $rootScope.$broadcast('user-update', data)
      })
      Api.socket.on('user-login', function (data) {
        $rootScope.USER = data
        $rootScope.$broadcast('user-login', data)
      })
      Api.socket.on('account-update', function (data) {
        $rootScope.ACCOUNT = data
        $rootScope.$broadcast('account-update', data)
        $rootScope.$applyAsync()
      })

      Api.socket.on('bet-error-retry', function (data) {
        $rootScope.$broadcast('bet-error-retry', data)
      })
      Api.socket.on('bet-placed', function (data) {
        $rootScope.ACCOUNT = data.account
        $rootScope.$broadcast('bet-placed', data)
        $rootScope.$applyAsync()
      })
      Api.socket.on('bet-error', function (data) {
        $rootScope.$broadcast('bet-error', data)
        Toast.error(data.error)
        $rootScope.$applyAsync()
      })
      Api.socket.on('bet-error-create', function (data) {
        $rootScope.$broadcast('bet-error-create', data)
      })
      Api.socket.on('withdraw-error', function (data) {
        $rootScope.$broadcast('withdraw-error', data)
        Toast.error('Withdrawal Failed: ' + data.error)
        $rootScope.$applyAsync()
      })
      Api.socket.on('withdraw-success', function (data) {
        $rootScope.ACCOUNT = data.account
        $rootScope.$broadcast('withdraw-success', data)
        Toast.error('Cashout Success. During beta, please wait for an admin to approve your withdrawal!')
        $rootScope.$applyAsync()
      })
      Api.socket.on('cashout-error', function (data) {
        $rootScope.$broadcast('cashout-error', data)
        Toast.error(data.error)
        $rootScope.$applyAsync()
      })
      Api.socket.on('cashout', function (data) {
        $rootScope.ACCOUNT = data.account
        $rootScope.$broadcast('cashout', data)
        $rootScope.$applyAsync()
      })
            // show an overlay, as the game cannot go on without a socket
      Api.socket.on('disconnect', function () {
        $rootScope.socketConnected = false
        $rootScope.$applyAsync()
      })
    }

    return Api
  }
})(window.angular)

/* global GA */
(function (window, angular) {
  'use strict'
  angular.module('app')
        .factory('GameAnalytics', GameAnalytics)
  GameAnalytics.$inject = []

  function GameAnalytics () {
    var config = {
      gameKey: '4df63eba443a6102363e267264b92f39',
      secretKey: '00b4fb36ddecd78da1d4e9a28db395136868e3d1',
      dataKey: '504c643f1eb48cf557b345edaaca83d54663b4a4',
      build: '0.0.1'
    }
    var user = new GA.User(Date.now().toString(), undefined, GA.Gender.male, 1984)
    var gaan = GA.getInstance()
    gaan.init(config.gameKey, config.secretKey, config.build, user)

    return GA
  }
})(window, window.angular)

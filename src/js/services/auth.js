(function (window, angular) {
  'use strict'

  angular.module('app')
        .factory('Auth', AuthFactory)

  AuthFactory.$inject = ['$rootScope', '$http', '$location', '$q', 'Api', '$localStorage', 'GameAnalytics']

  function AuthFactory ($rootScope, $http, $location, $q, Api, $localStorage, GA) {
    var Auth = { }
    function startGaSession () {
      var event = new GA.Events.User()
      GA.getInstance().addEvent(event)
    }
    function endGaSession () {
      var sessionDuration = (Date.now() - $localStorage.sessionStart) / 1000

      var event = new GA.Events.SessionEnd(
                sessionDuration // the length of the session in seconds
            )

      GA.getInstance().addEvent(event)
    }

    Auth.login = function (username, password) {
      var url = Api.url() + 'login'
      return $http.post(url, {
        username: username,
        password: password
      }).then(function (res) {
        var data = res.data
        Api.authHeader = data.authHeader
        $rootScope.USER = data.user
        $rootScope.ACCOUNT = data.account
        $rootScope.SESSION = data.session
        $localStorage.authHeader = data.authHeader
        $localStorage.USER = data.user
        $localStorage.SESSION = data.session
        $localStorage.ACCOUNT = data.account

        $localStorage.sessionStart = new Date()
        Api.connectSocket()
        startGaSession()
        return data
      }, function (res) {
        var err = res.data
        if (res.status === -1) {
          $rootScope.isConnectionFailed = true
          Api.connectSocket()
          endGaSession()
        }
        throw err
      })
    }

    Auth.signup = function (name, email, username, password, withdrawWallets) {
      var url = Api.url() + 'signup'
      return $http.post(url, {
        name: name,
        email: email,
        username: username,
        password: password,
        withdrawWallets: withdrawWallets
      }).then(function (res) {
        var data = res.data
        Api.authHeader = data.authHeader
        $rootScope.USER = data.user
        $rootScope.ACCOUNT = data.account
        $rootScope.SESSION = data.session
        $localStorage.authHeader = data.authHeader
        $localStorage.USER = data.user
        $localStorage.SESSION = data.session
        $localStorage.ACCOUNT = data.account
        $localStorage.sessionStart = new Date()
        startGaSession()
        Api.connectSocket()
        return data
      }, function (res) {
        if (res.status === -1) {
          $rootScope.isConnectionFailed = true
          endGaSession()
          Api.connectSocket()
        }
        var err = res.data
        throw err
      })
    }

    Auth.changePassword = function (oldPassword, newPassword, confirmPassword) {
      var url = Api.url() + 'users/my/password'
      var data = {
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword
      }
      return $http.put(url, data).then(function (res) {
        return res.data
      }, function (res) {
        var err = res.data
        throw err
      })
    }

    Auth.changeDetails = function (name, username, email) {
      var url = Api.url() + 'users/my'
      var data = {
        name: name,
        username: username,
        email: email
      }
      return $http.put(url, data).then(function (res) {
        return res.data
      }, function (res) {
        var err = res.data
        throw err
      })
    }

    Auth.changeAddress = function (newAddress) {
      var url = Api.url() + 'accounts/address'
      var data = {
        withdrawWallets: {burstcoin: newAddress}
      }
      return $http.put(url, data).then(function (res) {
        return res.data
      }, function (res) {
        var err = res.data
        throw err
      })
    }

    Auth.logout = function () {
      return $http.delete(Api.url() + 'logout').then(function () {
        Api.authHeader = null
        $rootScope.USER = null
        $rootScope.ACCOUNT = null
        $rootScope.SESSION = null
        $localStorage.authHeader = null
        $localStorage.USER = null
        $localStorage.SESSION = null
        $localStorage.ACCOUNT = null
        Api.connectSocket()
        endGaSession()
        return true
      }, function (res) {
        var err = res.data
        Api.authHeader = null
        $rootScope.USER = null
        $rootScope.ACCOUNT = null
        $rootScope.SESSION = null
        $localStorage.authHeader = null
        $localStorage.USER = null
        $localStorage.SESSION = null
        $localStorage.ACCOUNT = null

        endGaSession()
        Api.connectSocket()
        if (res.status === -1) {
          $rootScope.isConnectionFailed = true
        }
        throw err
      })
    }

    return Auth
  }
})(window, window.angular)

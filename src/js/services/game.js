(function (window, angular) {
  'use strict'
  angular.module('app')
        .factory('Game', Game)
  Game.$inject = ['$rootScope', '$http', '$state', 'Api', 'Toast']

  function Game ($rootScope, $http, $state, Api, Toast) {
    var Game = {
    }

    var processPlayer = function (data) {
      if (!data.user) {
            // the response data isn't a user, skip this
        return false
      }
      if (!data.game) {
        window.alert('GAME CONFIG PROGRAM ERROR. Make sure you report this!')
      }
    }

        /*
        */
    Game.getData = function (endpointId, transparent) {
      var url = Api.url() + endpointId

          // var endpointStripped = endpointId.replace("/","-");
          // var event = new GA.Events.Design(endpointStripped
              // 'Character:GET_'+endpointStripped   //A 1-5 part event id
          // );

          // GA.getInstance().addEvent(event);

      $rootScope.showLoading(transparent)
          // console.log("going to api: " + url, endpointId);
      return $http.get(url).then(function (res) {
        var data = res.data
        processPlayer(data)

        if (transparent) {
          $rootScope.hideLoadingTransparent(res.requestTime)
        } else {
          $rootScope.hideLoading(res.requestTime)
        }
        return data
      }, function (res) {
        if (transparent) {
          $rootScope.hideLoadingTransparent(99999)
        } else {
          $rootScope.hideLoading(99999)
        }
        var err = res.data
        if (res.status === -1) {
          $rootScope.isConnectionFailed = true
        }
        Toast.error(err)
        throw err
      })
    }
        //
        // */
    Game.postData = function (endpointId, payload, transparent) {
      var url = Api.url() + endpointId

          // var endpointStripped = endpointId.replace("/","-");
          // var event = new GA.Events.Design(endpointStripped
              // 'Character:GET_'+endpointStripped   //A 1-5 part event id
          // );

          // GA.getInstance().addEvent(event);

      $rootScope.showLoading(transparent)
      return $http.post(url, payload).then(function (res) {
        var data = res.data
        processPlayer(data)

        if (transparent) {
          $rootScope.hideLoadingTransparent(res.requestTime)
        } else {
          $rootScope.hideLoading(res.requestTime)
        }
        return data
      }, function (res) {
        var err = res.data

        if (transparent) {
          $rootScope.hideLoadingTransparent(999999)
        } else {
          $rootScope.hideLoading(99999)
        }
        if (res.status === -1) {
          $rootScope.isConnectionFailed = true
        }
        Toast.error(err)
        throw err
      })
    }

    return Game
  }
})(window, window.angular)

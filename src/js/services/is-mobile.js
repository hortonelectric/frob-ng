(function (angular) {
  'use strict'

  var app = angular.module('app')

  app.provider('isMobile', function isMobileProvider () {
    this.isMobile = {
      sizeLessEquals768: function () {
        return document.body.clientWidth <= 768
      },
      sizePortrait: function () {
        return document.body.clientWidth < document.body.clientHeight
      },
      Android: function () {
        return navigator.userAgent.match(/Android/i)
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i)
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPod|iPad/i)
      },
      ipad: function () {
        return navigator.userAgent.match(/iPad/i)
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i)
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i)
      },
      Firefucked: function () {
        return navigator.userAgent.match(/Firefox/i)
      },
      Edge: function () {
        return navigator.userAgent.match(/Edge/i)
      },

      any: function () {
        return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows())
      }
    }

    this.$get = [function isMobileFactory () {
      return this.isMobile
    }]
  })
})(window.angular)

(function (window, angular) {
  'use strict'

  angular.module('app').directive('changeaddress',
    ['$q', '$rootScope', 'Toast', '$location', '$filter', 'Auth',
      function ($q, $rootScope, Toast, $location, $filter, Auth) {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/changeaddress.html',
          link: function ($scope) {
            $rootScope.$watch('ACCOUNT', function () {
              if ($rootScope.ACCOUNT) {
                $scope.burstcoinaddress = $filter('withdrawAddress')($rootScope.ACCOUNT.withdrawWallets, 'burstcoin')
              }
            })
            $scope
                .changeAddress = function () {
                  Auth.changeAddress(
                  $scope.burstcoinaddress
                  )
                .then(function () {
                  Toast.error('Successfully updated BURST address')
                }, function (err) {
                  Toast.error(err)
                })
                }
          }
        }
      }])
})(window, window.angular)

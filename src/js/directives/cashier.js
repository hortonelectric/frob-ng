(function (window, angular) {
  'use strict'

  angular.module('app').directive('cashier', cashier)
  cashier.$inject = ['$rootScope', 'Api', 'Game']

  function cashier ($rootScope, Api, Game) {
    var directive = {
      restrict: 'E',
      scope: '=',
            // controller: stateParamsCtrl,
      templateUrl: 'directives/cashier.html',
      link: function ($scope) {
        $scope.Api = {protocol: Api.protocol, host: Api.host, basePath: Api.basePath}
        $scope.depositAddresses = {}
        $scope.buttons = {}

        $scope.withdraw = function () {
          $scope.buttons.withdrawDisabled = true
          Api.socket.emit('withdraw')
        }

        $scope.$on('withdraw-error', function () {
          $scope.buttons.withdrawDisabled = false
        })
        $scope.$on('withdraw-success', function () {
          $scope.buttons.withdrawDisabled = false
        })
        $scope.deposit = function (currency) {
          $scope.depositAddresses = {}
          Game.getData('deposit/' + currency, true).then(function (result) {
            $scope.depositAddresses[currency] = result.address
          })
        }

        $scope.init = function () {
          Game.getData('withdraws/my').then(function (result) {
            $scope.withdraws = result
          })
          Game.getData('deposits/my').then(function (result) {
            $scope.deposits = result
          })
        }

        $scope.shapeshift_click = function (a, e) {
          e.preventDefault()
          Game.getData('deposit/bitcoin', true).then(function (result) {
            window.open(
                    'https://shapeshift.io/shifty.html?destination=' + result.address + '&amp;output=BTC&amp;apiKey=9b9c6db5101b33a02fd7103ea03095a7ca8cc177f15daacf81727793ad932f1fed8c3413397319b7b53de60502830516d1911432b9f3b3c219e7b290cd10b18d&amp;amount=',
                    '1418115287605', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=0,left=0,top=0'
                  )
          })

          return false
        }

        $scope.init()
      }
    }
    return directive
  }
})(window, window.angular)

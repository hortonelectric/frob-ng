(function (window, angular) {
  'use strict'

  angular.module('app').directive('leaderboard', leaderboard)
  leaderboard.$inject = ['Game']

  function leaderboard (Game) {
    var directive = {
      restrict: 'E',
      scope: '=',
            // controller: stateParamsCtrl,
      templateUrl: 'directives/leaderboard.html',
      link: function ($scope) {
        $scope.init = function () {
          Game.getData('leaderboard').then(function (data) {
            $scope.leaderboard = data
          })
        }
        $scope.init()
      }
    }
    return directive
  }
})(window, window.angular)

(function (window, angular) {
  'use strict'

  angular.module('app').directive('stats', stats)
  stats.$inject = []

  function stats () {
    var directive = {
      restrict: 'E',
      scope: '=',
            // controller: stateParamsCtrl,
      templateUrl: 'directives/stats.html',
      link: function ($scope) {
        $scope.init = function () {

        }
        $scope.init()
      }
    }
    return directive
  }
})(window, window.angular)

/* global _, screen */
(function (window, angular, _) {
  'use strict'

  angular.module('app').directive('gameIndex', gameIndex)
  gameIndex.$inject = ['$rootScope', '$filter', 'Api', 'Toast']

  function gameIndex ($rootScope, $filter, Api, Toast) {
    var directive = {
      restrict: 'E',
      scope: '=',
            // controller: stateParamsCtrl,
      templateUrl: 'directives/game/index.html',
      link: function ($scope) {
        $scope.data = {currentView: 'chat'}
        $scope.chatForm = {}
        $scope.chats = []
        $scope.nextBet = {}
        $scope.betActive = false
        $scope.gameActive = false
        $scope.countdownOn = false
        $scope.gameDetails = null
        $scope.betEnabled = true
        $scope.bet = {amountNQT: (1).toSatoshi(), amount: 1, autoCashout: 2}

        $scope.$watch('bet.amount', function (value) {
          $scope.bet.amountNQT = parseFloat(value).toSatoshi()
        })
        $scope.hideGame = function () {
          $scope.gameDetails = null
        }
        $scope.viewGame = function (round) {
          $scope.gameDetails = JSON.stringify(round, null, '    ')
        }
        $scope.betText = function () {
          if ($scope.betActive) {
            if ($scope.gameActive) {
              return 'CASHOUT'
            } else {
              return 'WAIT...'
            }
          } else if ($scope.gameActive) {
            if ($scope.nextBet.amountNQT) {
              return 'CANCEL'
            } else {
              return 'BET'
            }
          } else if ($scope.countdownOn) {
            return 'BET'
          } else { return 'WAIT...' }
        }
        $scope.placeBet = function (bet) {
          $scope.betEnabled = false
          if (!parseFloat(bet.amount)) {
            Toast.error('No bet amount specified')
            return false
          }
          if (!$rootScope.ACCOUNT) {
            Toast.error('Not logged in! Log in to place bets')
            return false
          }
          for (var i = 0; i < $rootScope.ACCOUNT.withdrawWallets.length; i++) {
            if ($rootScope.ACCOUNT.withdrawWallets[i].currency === 'burstcoin') {
              var cryptovalue = $filter('cryptovalue')
              return cryptovalue($rootScope.ACCOUNT.withdrawWallets[i].balance)
            }
            continue
          }
          console.log('placing bet', bet)
          Api.socket.emit('place-bet', bet)
        }
        $scope.handleBetButton = function () {
          if (!$scope.betEnabled) { return false }
          if ($scope.betActive) {
            if ($scope.gameActive) {
              Api.socket.emit('cashout')
            }
          } else if ($scope.gameActive) {
            if ($scope.nextBet.amountNQT) {
                    // already set, so unset it
              $scope.nextBet = {}
            } else {
              $scope.nextBet = _.cloneDeep($scope.bet)
            }
          } else if ($scope.countdownOn) {
            $scope.placeBet($scope.bet)
          } else {
                  // $scope.placeBet($scope.bet);
          }
        }
        $scope.$on('game-countdown-start', function () {
          $scope.countdownOn = true
          if ($scope.nextBet.amountNQT) {
            $scope.placeBet($scope.nextBet)
            $scope.nextBet = {}
          }
          $rootScope.$applyAsync()
        })
        $scope.$on('game-start', function () {
          $scope.gameActive = true
          $scope.countdownOn = false
          $rootScope.$applyAsync()
        })
        $scope.$on('game-heartbeat', function () {
          $scope.gameActive = true
          $rootScope.$applyAsync()
        })
        $scope.$on('cashout', function () {
          $scope.betActive = false
          $rootScope.$applyAsync()
        })
        $scope.$on('cashout-error', function () {

        })
        $scope.$on('game-bust', function () {
          $scope.betActive = false
          $scope.gameActive = false
          $rootScope.$applyAsync()
        })
        $scope.$on('game-countdown', function () {
          $scope.gameActive = false
          $rootScope.$applyAsync()
        })
        $scope.$on('bet-error-retry', function () {
          Api.socket.emit('place-bet', $scope.bet)
        })
        $scope.$on('bet-error-create', function (props, data) {
          Toast.error(data.error)
          $scope.betEnabled = true
          $rootScope.$applyAsync()
        })
        $scope.$on('bet-placed', function () {
          console.log('bet placed')
          $scope.betActive = true
          $scope.betEnabled = true
          $rootScope.$applyAsync()
        })
        $scope.$watch('data.currentView', function (newVal) {
          if (newVal === 'chat') {
            setTimeout(function () {
              $scope.$apply(function () {
                if (document.getElementById('chat-window')) {
                  document.getElementById('chat-window').scrollTop = document.getElementById('chat-window').scrollHeight + 9999
                }
              })
            }, 300)
          }
        })

        $scope.$on('windowResize', function () {
          $scope.blurTable()
          if ($rootScope.isMobile.Android()) {
            var isSoftkeyboardOpen = Math.min(window.innerWidth / screen.width, window.innerHeight / screen.height) < 0.7

            if (!isSoftkeyboardOpen) {
              document.getElementById('chat-input').blur()
            }
          }
        })

        $scope.$on('game-info', function (props, data) {
          if ($scope.chats.length && data.chats.length && _.last($scope.chats).uuid === _.last(data.chats).uuid) {
            return false
          }
          $scope.chats = data.chats
          $scope.$apply()
          if (document.getElementById('chat-window')) {
            document.getElementById('chat-window').scrollTop = document.getElementById('chat-window').scrollHeight + 9999
          }
        })
        $scope.sendChat = _.throttle(function () {
          $scope.sendChat.cancel()
          var playerName = 'Anon'
          if ($rootScope.USER) {
            playerName = $rootScope.USER.username
          }
          Api.socket.emit('send-chat', {uuid: Api.fingerprint.hash + new Date().getTime(true), username: playerName, text: $scope.chatForm.chat})
          $scope.chatForm.chat = ''
        }, 500, {leading: true})
        var keyHandler = function () {
          return true
        }; var keyHandlerB = function () {
          return true
        }
        var unbindKeys = function () {
          angular.element(document.body).unbind('keyup', keyHandlerB)
          angular.element(document.body).unbind('keyup', keyHandler)
        }

        var bindKeys = function () {
          unbindKeys()
          angular.element(document.body).bind('keyup', keyHandler)
        }
        var bindKeysB = function () {
          unbindKeys()
          angular.element(document.body).bind('keyup', keyHandlerB)
        }

        $scope.$on('$destroy', function () {
          angular.element(document.body).unbind('keyup', keyHandler)
        })
        $scope.init = function () {
          unbindKeys()
          bindKeys()
          bindKeysB()
        }
        $scope.init()
      }
    }
    return directive
  }
})(window, window.angular, _)

/* global Konva */
(function (window, angular) {
  'use strict'

  angular.module('app').directive('konvaCanvas', konvaCanvas)
  konvaCanvas.$inject = ['$rootScope', '$filter']

  function konvaCanvas ($rootScope, $filter) {
    var directive = {
      restrict: 'E',
      scope: '=',
            // controller: stateParamsCtrl,
      template: '<div id="game-canvas"></div>',
      link: function ($scope) {
        var burstNumberTimer
        var stage, layer
        var mainText
        var winningsText, burstingLabelText, maxWinText, winningsBg
                // slow down if the client cannot get messages from
                // the server at 250ms, the normal rate.
        var fps = 333
                // if($rootScope.isMobile.any()) {
                //   fps = 1000;
                // }

        function centerMainText () {
          mainText.setOffset({x: mainText.getWidth() / 2, y: mainText.getHeight() / 2})
        }
        function centerSupportingText () {
          winningsText.setOffset({x: winningsText.getWidth() / 2, y: winningsText.getHeight() / 2})

          winningsBg.setOffset({x: winningsBg.getWidth() / 2, y: winningsBg.getHeight() / 2})
          maxWinText.setOffset({x: maxWinText.getWidth() / 2, y: maxWinText.getHeight() / 2})
          burstingLabelText.setOffset({x: burstingLabelText.getWidth() / 2, y: burstingLabelText.getHeight() / 2})
        }
        function writeStringResult () {
          var numResult = (($scope.currentResult + 100) / 100)
                      // console.log($scope.currentResult,$scope.resultTarget,stringResult);
          var stringResult = numResult.toString()
          if (stringResult.split('.').length < 2) {
            stringResult = stringResult + '.00'
          } else if (stringResult.toString().split('.')[1].length < 2) {
            stringResult = stringResult + '0'
          }
                      // console.log(result+"x",mainText);
          mainText.text(stringResult + 'x')
          centerMainText()
        }
        function writeWinningsResult () {
                  // player already cashed out, this becomes static
          var winningsAmt = 0
          if ($scope.winnings > 0 && !$scope.stake) {
            winningsAmt = $scope.winnings
          } else {
            var numResult = (($scope.currentResult + 100) / 100)
            winningsAmt = ($scope.stake * numResult).toFixedNumber(2)
          }
          winningsText.text('Won: ' + winningsAmt + ' BURST')
          if (winningsAmt) {
            layer.add(winningsBg)
            layer.add(winningsText)
          } else {
            winningsBg.remove()
            winningsText.remove()
          }

          centerSupportingText()
        }
        function writeMaxWinnings (maxWinnings) {
          maxWinText.text('Max Winnings: ' + maxWinnings + ' BURST')
          centerSupportingText()
        }
        function burstNumber () {
          if ($scope.resultTarget <= 1 || $scope.currentResult >= $scope.resultTarget) {
                      // we don't need to do anything, either the current result has not changed, or it's already at the target
                      // console.log('why',$scope.currentResult,$scope.resultTarget);
            return false
          } else {
                      // console.log($scope.currentResult,$scope.resultTarget);
            $scope.currentResult += $scope.resultinc
            if ($scope.currentResult > $scope.resultTarget) {
              $scope.currentResult = $scope.resultTarget
            }
            writeStringResult()
            writeWinningsResult()
            layer.draw()
            if ($scope.looptime > fps) {
              burstNumberTimer = setTimeout(burstNumber, $scope.looptime)
            } else {
              burstNumberTimer = setTimeout(burstNumber, fps)
            }
          }
        }

        function load () {
          burstNumberTimer = setTimeout(function () {

          }, 1000)
          if (stage && stage.destroy) {
            stage.destroy()
          }
          var width = 1920
          var height = 1080
          if ($rootScope.isMobile.ipad()) {
            width = 1440; height = 1440
          }
                    // Konva.pixelRatio = 1;
          stage = new Konva.Stage({
            container: 'game-canvas',
            width: width,
            height: height
          })

                    // add canvas element
          layer = new Konva.FastLayer()
          stage.add(layer)

          mainText = new Konva.Text({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2 - 90,
            text: 'Loading...',
            fontSize: 300,
            fontFamily: '"Nova Mono", monospace, Courier New, Courier, Fixed',
            fill: '#00FF00',
            shadowColor: 'black',
            shadowBlur: 7,
            shadowOffset: {x: 3, y: 3},
            shadowOpacity: 0.5,
            listening: false,
            perfectDrawEnabled: false
          })

                    // since this text is inside of a defined area, we can center it using
                    // align: 'center'
          winningsText = new Konva.Text({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2 + 120,
            text: 'Won: 0 BURST',
            fontSize: 90,
            fontFamily: 'Dosis, "Arial Narrow", "Helvetica Condensed", Arial, Helvetica, sans-serif',
            fill: '#CCC',
            width: 1600,
            padding: 15,
            align: 'center',
            listening: false,
            perfectDrawEnabled: false

          })

          burstingLabelText = new Konva.Text({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2 - 260,
            text: 'BURSTING',
            fontSize: 60,
            fontFamily: 'Dosis, "Arial Narrow", "Helvetica Condensed", Arial, Helvetica, sans-serif',
            fill: '#CCCCCC',
            width: 1000,
            padding: 20,
            align: 'center',
            listening: false,
            perfectDrawEnabled: false

          })

          maxWinText = new Konva.Text({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2 + 260,
            text: 'Max Winnings: 0 BURST',
            fontSize: 60,
            fontFamily: 'Dosis, "Arial Narrow", "Helvetica Condensed", Arial, Helvetica, sans-serif',
            fill: '#AAAAAA',
            width: 1000,
            padding: 0,
            align: 'center',
            listening: false,
            perfectDrawEnabled: false

          })

          winningsBg = new Konva.Rect({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2 + 120,
            stroke: '#333',
            strokeWidth: 5,
            fill: '#222',
            width: 1600,
            height: winningsText.getHeight(),
            shadowColor: 'black',
            shadowBlur: 10,
            shadowOffset: [10, 10],
            shadowOpacity: 0.5,
            cornerRadius: 100,
            listening: false,
            perfectDrawEnabled: false

          })

          centerMainText()
          centerSupportingText()
          layer.add(mainText)
                    // layer.add(winningsBg);
                    // layer.add(winningsText);
          layer.add(maxWinText)
          layer.add(burstingLabelText)
          layer.draw()

          $scope.resultinc = 1
          $scope.currentResult = 0
          $scope.resultTarget = -1
          $scope.$on('game-bust', function (props, data) {
            $scope.stake = 0
            mainText.fill('#ff0000')
            burstingLabelText.text('BURST @')
            centerSupportingText()
            $scope.targetResult = parseInt(data, 10)
            $scope.currentResult = parseInt(data, 10)
            clearTimeout(burstNumberTimer)
            writeStringResult()
            writeWinningsResult()
            layer.draw()
            $scope.resultinc = 1
            $scope.currentResult = 0
            $scope.resultTarget = -1
          })
          $scope.$on('game-start', function () {
            $scope.winnings = 0
            $scope.resultinc = 1
            $scope.currentResult = 0
            $scope.resultTarget = -1
            burstingLabelText.text('BURSTING')
                      // layer.add(winningsBg);
                      // layer.add(winningsText);
            centerSupportingText()
            mainText.fill('#00ff00')
            mainText.text('0.00x')
            centerMainText()
            layer.draw()
          })

          $scope.$on('game-heartbeat', function (props, data) {
                        // we will write the working result -1 to the screen,
                        // so that the bust will work properly if the bust is on the next frame,
            $scope.currentResult = parseInt($scope.resultTarget, 10) - 1

                        // now the target for the next frame will be the current result from the server
            $scope.resultTarget = parseInt(data.workingResult, 10)

                        // console.log("server says",JSON.stringify(data),$scope.currentResult,$scope.resultTarget);

            $scope.resultinc = parseInt(data.resultinc, 10)
            $scope.looptime = parseInt(data.looptime, 10)

            burstNumber()
          })
          $scope.$on('game-countdown', function (props, data) {
            $scope.winnings = 0
            burstingLabelText.text('Next Game:')
            mainText.fill('#cccccc')
            mainText.text(data)
            centerMainText()
            layer.draw()
          })

          $scope.$on('cashout', function (props, data) {
            $scope.stake = 0
            $scope.winnings = data.bet.satoshiWon.toBitcoin() || 0
            writeWinningsResult()
          })
          $scope.$on('bet-placed', function (props, data) {
            $scope.stake = data.bet.satoshiBet.toBitcoin()
            writeWinningsResult()
            layer.draw()
          })

          $scope.$on('game-info', function (props, data) {
            writeMaxWinnings($filter('cryptovalue')(data.maxWinnings))
          })
          $scope.$on('game-countdown-start', function () {
            $scope.stake = 0
            $scope.winnings = 0
            writeWinningsResult()
          })

          burstNumber()
        }

        if (document.fonts) {
          document.fonts.ready.then(function () { load() })
        } else {
          setTimeout(function () {
            load()
          }, 5000)
        }
      }
    }
    return directive
  }
})(window, window.angular)

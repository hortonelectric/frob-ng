(function (window, angular) {
  'use strict'

  angular.module('app').directive('login',
    ['$q', 'Toast', '$location', 'Auth',
      function ($q, Toast, $location, Auth) {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/login.html',
          link: function ($scope) {
            $scope.login = function () {
              Auth.login($scope.username, $scope.password).then(function () {
                $location.path('/')
              }, function (err) {
                Toast.error(err)
                return $q.reject(err)
              })
            }
          }
        }
      }])
})(window, window.angular)

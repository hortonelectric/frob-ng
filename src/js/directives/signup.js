(function (window, angular) {
  'use strict'

  angular.module('app').directive('signup',
    ['$q', 'Toast', '$location', 'Auth',
      function ($q, Toast, $location, Auth) {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/signup.html',
          link: function ($scope) {
            $scope.signup = function () {
              $scope.wallets = {burstcoin: $scope.burstcoin, bitcoin: $scope.bitcoin}
              Auth.signup($scope.name, $scope.email, $scope.newUsername, $scope.newPassword, $scope.wallets).then(function () {
                $location.path('/')
              }, function (err) {
                Toast.error(err)
                return $q.reject(err)
              })
            }
          }
        }
      }])
})(window, window.angular)

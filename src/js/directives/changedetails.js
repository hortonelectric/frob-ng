(function (window, angular) {
  'use strict'

  angular.module('app').directive('changedetails',
    ['$q', '$rootScope', 'Toast', '$location', '$filter', 'Auth',
      function ($q, $rootScope, Toast, $location, $filter, Auth) {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/changedetails.html',
          link: function ($scope) {
            $rootScope.$watch('ACCOUNT', function () {
              if ($rootScope.ACCOUNT) {
                $scope.name = $rootScope.USER.roles.account.name
                $scope.username = $rootScope.USER.username
                $scope.email = $rootScope.USER.email
              }
            })
            $scope
                .changeDetails = function () {
                  Auth.changeDetails(
                  $scope.name,
                  $scope.username,
                  $scope.email
                  )
                .then(function () {
                  Toast.error('Successfully updated details')
                }, function (err) {
                  Toast.error(err)
                })
                }
          }
        }
      }])
})(window, window.angular)

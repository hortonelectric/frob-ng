(function (window, angular) {
  'use strict'

  angular.module('app').directive('blank', blank)
  blank.$inject = []

  function blank () {
    var directive = {
      restrict: 'E',
      scope: '=',
            // controller: stateParamsCtrl,
      templateUrl: 'directives/blank.html',
      link: function ($scope) {
        $scope.init = function () {

        }
        $scope.init()
      }
    }
    return directive
  }
})(window, window.angular)

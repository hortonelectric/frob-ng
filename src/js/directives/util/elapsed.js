(function (window, angular, moment) {
  'use strict'

  moment.relativeTimeThreshold('h', 49)
  moment.relativeTimeThreshold('m', 60)

  angular.module('app').directive('elapsed', function () {
    return {
      restrict: 'EA',
      scope: {
        date: '=',
        refDate: '='
      },
      template: '<span>{{elapsed}}</span>',
      link: function (scope) {
        scope.updateTime = function () {
          var ref = moment(scope.refDate).utc()
          scope.elapsed = ref.to(moment(scope.date).utc())
        }

        setInterval(function () {
          scope.$apply(function () {
            scope.updateTime()
          })
        }, (60 * 1000))

        scope.$watch('date', scope.updateTime)
      }
    }
  })
})(window, window.angular, window.moment)

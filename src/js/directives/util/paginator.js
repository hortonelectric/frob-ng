(function (window, angular) {
  'use strict'

  angular.module('app').directive('paginator', [function () {
    return {
      template: '<uib-pagination total-items="total"' +
                'ng-model="page" ' +
                'ng-change="onChange(page)" ' +
                'previous-text="&lsaquo;" ' +
                'next-text="&rsaquo;" ' +
                'first-text="&laquo;" ' +
                'last-text="&raquo;" ' +
                'max-size="8" ' +
                'boundary-links="true"></uib-pagination>',
      restrict: 'E',
      scope: {
        changeFn: '&update',
        total: '='
      },
      link: function ($scope) {
        $scope.page = 1
        $scope.onChange = function (page) {
          $scope.changeFn({page: page})
        }
      }
    }
  }])
})(window, window.angular)

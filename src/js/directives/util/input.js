(function (window, angular) {
  'use strict'

  angular.module('app').directive('inp',
    [
      function () {
        return {
          restrict: 'E',
          scope: '=',
          replace: true,
          template: '<input autocapitalize="none" autocorrect="off" autocomplete="off"/>',
          link: function ($scope) {
            $scope.nothing = true
          }
        }
      }])
})(window, window.angular)

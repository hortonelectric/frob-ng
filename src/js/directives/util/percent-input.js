(function (window, angular) {
  'use strict'

  angular.module('app').directive('percentInput', [function () {
    return {
      template: '<input class="form-control crypto-input" ng-disabled="disabled" type="number" min="0" max="100" step="0.1" ng-model="displayVal">' +
                '<span class="input-group-addon crypto-input-label">%</span>',
      restrict: 'E',
      scope: {
        intVal: '=model',
        disabled: '=ngDisabled',
        max: '='
      },
      link: function (scope) {
        scope.$watch('intVal', function (newVal) {
          var val = parseFloat(newVal)
          if (isNaN(val)) {

          } else {
            scope.displayVal = parseFloat((val * 100).toFixed(2))
          }
        })
        scope.$watch('displayVal', function (newVal) {
          var val = parseFloat(newVal)
          if (isNaN(val)) {

          } else {
            var intVal = parseFloat((val / 100).toFixed(4))
            if (intVal > scope.max) {
              intVal = scope.max
            }
            scope.intVal = intVal
            scope.displayVal = parseFloat((intVal * 100).toFixed(2))
          }
        })
        scope.$watch('max', function (newVal) {
          var val = parseFloat(newVal)
          if (isNaN(val)) {

          } else {
            if (scope.intVal > scope.max) {
              scope.intVal = scope.max
              scope.displayVal = parseFloat((scope.intVal * 100).toFixed(2))
            }
          }
        })
      }
    }
  }])
})(window, window.angular)

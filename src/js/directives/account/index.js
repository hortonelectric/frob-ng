(function (window, angular) {
  'use strict'

  angular.module('app').directive('accountIndex',
    [
      function () {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/account/index.html',
          link: function ($scope) {
            $scope.init = function () {

            }

            $scope.init()
          }
        }
      }])
})(window, window.angular)

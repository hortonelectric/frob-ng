(function (window, angular) {
  'use strict'

  angular.module('app').directive('navbar',
    [
      function () {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/navbar.html',
          link: function ($scope) {
            $scope.init = function () {

            }

            $scope.init()
          }
        }
      }])
})(window, window.angular)

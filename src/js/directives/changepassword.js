(function (window, angular) {
  'use strict'

  angular.module('app').directive('changepassword',
    ['$q', 'Toast', '$location', 'Auth',
      function ($q, Toast, $location, Auth) {
        return {
          restrict: 'E',
          scope: '=',
          templateUrl: 'directives/changepassword.html',
          link: function ($scope) {
            $scope
                .changePassword = function () {
                  Auth.changePassword(
                    $scope.oldPassword,
                    $scope.newPassword,
                    $scope.confirmPassword
                  )
                .then(function () {
                  // $location.path("/");
                  Toast.error('Successfully updated password')
                }, function (err) {
                  if (err.message === 'child "confirmPassword" fails because ["confirmPassword" must be one of [ref:newPassword]]') {
                    Toast.error('Re-enter password must match your new password')
                  } else {
                    Toast.error(err)
                  }
                    // return $q.reject(err);
                })
                }
          }
        }
      }])
})(window, window.angular)

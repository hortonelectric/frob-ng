(function (angular) {
  'use strict'

  var app = angular.module('app')

  app.constant('NAV_CONFIG', [{
    title: {en_US: 'Login / Register'},
    prefix: '',
    hideMenu: false,
    requireNotUser: true,
    paths: [{
      title: {en_US: 'Login / Register'},
      href: '/home',
      single: true,
      states: [{
        name: 'homepage',
        conf: {
          templateUrl: '.html',
          controller: ['$rootScope', '$location', function ($rootScope, $location) {
            if ($rootScope.ACCOUNT && $rootScope.ACCOUNT.name) {
              $location.path('/')
            }
          }]
        }
      }]
    }]
  }, {
    title: {en_US: 'Play Now'},
    prefix: '',
    single: true,
    requireUser: false,
    paths: [{
      title: {en_US: 'Play'},
      href: '/',
      single: true,
      states: [{
        name: 'game-home',
        conf: {
          templateUrl: 'game.html'
        }
      }]
    }]
  }, {
    title: {en_US: 'Account'},
    requireUser: true,
    prefix: '',
    paths: [{
      href: '/account',
      single: true,
      requireUser: true,
      states: [{
        name: 'account',
        conf: {
          templateUrl: '.html',
          resolve: {
          }
        }
      }]
    }]
  }, {
    title: {en_US: 'Logout'},
    prefix: '',
    requireUser: true,
    paths: [{
      href: '/logout',
      single: true,
      requireUser: true,
      states: [{
        name: 'logout',
        conf: {
          // since our prefix here is "", this
          // url will resolve as '/logout'
          template: '<h1></h1>',
          controller: ['$scope', '$location', 'Auth', function ($scope, $location, Auth) {
            Auth.logout().then(function () {
              $location.path('/home')
            }, function () {
              $location.path('/home')
            })
          }]
        }
      }]
    }]
  }, {
    title: {en_US: 'FAQ'},
    prefix: '',
    href: 'http://made2burst.com/faq/',
    external: true
  }, {
    title: {en_US: 'Cashier'},
    prefix: '',
    requireUser: true,
    paths: [{
      href: '/cashier',
      single: true,
      requireUser: true,
      states: [{
        name: 'cashier',
        conf: {
          templateUrl: '.html',
          resolve: {
          }
        }
      }]
    }]
  }, {
    title: {en_US: 'Leaderboard'},
    prefix: '',
    paths: [{
      href: '/leaderboard',
      single: true,
      states: [{
        name: 'leaderboard',
        conf: {
          templateUrl: '.html',
          resolve: {
          }
        }
      }]
    }]
  }, {
    title: {en_US: 'Stats'},
    prefix: '',
    paths: [{
      href: '/stats',
      single: true,
      states: [{
        name: 'stats',
        conf: {
          templateUrl: '.html',
          resolve: {
          }
        }
      }]
    }]
  }, {
    title: {en_US: 'Support'},
    prefix: '',
    href: 'http://made2burst.com/support/',
    external: true
  }])
})(window.angular)

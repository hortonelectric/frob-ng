(function (angular) {
  'use strict'

  angular.module('app')
        .config(config)

  config.$inject = ['$compileProvider', '$locationProvider',
    '$stateProvider', '$urlRouterProvider',
    '$httpProvider', 'ngToastProvider', 'NAV_CONFIG',
    'cfpLoadingBarProvider']

  function config ($compileProvider, $locationProvider, $stateProvider,
                     $urlRouterProvider, $httpProvider, ngToastProvider, NAV_CONFIG, cfpLoadingBarProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|bitcoin|burstcoin):/)

    // use html5 mode path handling
    $locationProvider.html5Mode(true).hashPrefix('!')

    $urlRouterProvider.otherwise('/')

    NAV_CONFIG.forEach(function (navGroup) {
      if (navGroup.state) {
        var conf = angular.copy(navGroup.state.conf || {})
                // these can only be abstract states
        conf.abstract = true
        var pathStr = navGroup.prefix
                // no urls here!
                // delete conf.url;
        if (conf.templateUrl) {
                    // if we have templateUrl, append it to the pathStr
          conf.templateUrl = 'tpl' + pathStr + conf.templateUrl
        } else if (!conf.template) {
                    // otherwise, if a template string/function is not provided, use index.html
          conf.templateUrl = 'tpl' + pathStr + '/index.html'
        }
        $stateProvider.state(navGroup.state.name, conf)
      }
      if (navGroup.paths) {
        navGroup.paths.forEach(function (path) {
          path.states.forEach(function (state) {
            if (!state.conf) {
              state.conf = {}
            }
            var conf = angular.copy(state.conf)
            var pathStr = navGroup.prefix + path.href
                        // if no url is provided, generate
                        // from prefix and href
            if (!conf.url) {
              conf.url = pathStr
            }
            if (conf.templateUrl) {
                            // if we have templateUrl, append it to the pathStr
              conf.templateUrl = 'tpl' + pathStr + conf.templateUrl
            } else if (!conf.template && !conf.views) {
                            // otherwise, if a template
                            // string/function or views is/are not
                            // provided, use index.html
              conf.templateUrl = 'tpl' + pathStr + '/index.html'
            }
            if (conf.views) {
              Object.keys(conf.views).forEach(function (viewName) {
                var view = conf.views[viewName]
                if (view.templateUrl) {
                                    // if we have templateUrl, append it to the pathStr
                  view.templateUrl = 'tpl' + pathStr + view.templateUrl
                } else if (!view.template) {
                                    // otherwise, if a template
                                    // string/function or views is/are not
                                    // provided, use index.html
                  view.templateUrl = 'tpl' + pathStr + '/index.html'
                }
              })
            }
            $stateProvider.state(state.name, conf)
          })
        })
      } else if (navGroup.state) {
        $stateProvider.state(navGroup.state.name, navGroup.state.conf)
      }
    })

        // show toast error on all http errors
    var interceptor = function ($q, $scope, $location, Api) {
      return {
        requestError: function (rejection) {
          var data = rejection.data
          if (rejection.status < 0) {
            $scope.isConnectionFailed = true
          }
          if (!data) {
            data = {error: rejection.statusText, message: rejection.statusText}
          }
          data.originalMessage = data.message

          $scope.$broadcast('http error', rejection)
          return $q.reject(rejection)
        },
        'request': function (config) {
          if (Api.authHeader && config.headers.Authorization === undefined) {
            config.headers.Authorization = Api.authHeader
          }
          config.timeout = 5000
          interceptor.startTime = new Date()
          return config || $q.when(config)
        },
        'response': function (response) {
          response.endTime = new Date()
          response.requestTime = response.endTime.getTime(1) - interceptor.startTime.getTime(1)
          return response || $q.when(response)
        },
        'responseError': function (rejection) {
          if (rejection.status === 403 || rejection.status === 401) {
            $location.path('/')
          } else if (rejection.status === -1) {
            $scope.isConnectionFailed = true
          }
          var data = rejection.data
          if (!data) {
            data = {error: rejection.statusText, message: rejection.statusText}
          }
          data.originalMessage = data.message

          $scope.$broadcast('http error', rejection)

          return $q.reject(rejection)
        }

      }
    }

    $httpProvider.interceptors.push(['$q', '$rootScope', '$location', 'Api', interceptor])

    ngToastProvider.configure({
      animation: 'fade',
      horizontalPosition: 'center',
      verticalPosition: 'top'
    })

    cfpLoadingBarProvider.includeSpinner = false
    cfpLoadingBarProvider.parentSelector = '#loading-bar-target'
  }
})(window.angular)

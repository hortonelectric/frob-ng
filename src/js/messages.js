(function (window, angular) {
  'use strict'

  angular.module('app').constant('MESSAGES', {
    'success': '<%- messages.success %>'
  })
})(window, window.angular)

#### Installing Docker

For installing docker follow the official documentation [here](https://docs.docker.com/)
  Guide for installing Docker on [Mac](https://docs.docker.com/docker-for-mac/)
  Guide for installing Docker on [Windows](https://docs.docker.com/docker-for-windows/)
  Guide for installing Docker in [Linux](https://docs.docker.com/engine/installation/linux/ubuntu/)

#### Building the docker image.

Docker image can be built using docker-compose command.

Note: Before you build the docker image, make sure you have edited the `.env` file and have adjusted the ENV variables

To build the image run the following command

```
docker-compose build
```

#### Running the docker image.

To run the docker container:

```
docker-compose up
```

#### Rebuilding the docker image.

```
docker-compose up --rebuild
```

### Note on ENV variables

* `DOCKER_HOST`: hostname via which docker containers are going to be accessed.
* `PORT`: Port over which the app will be accessed
* `SSL_PORT`: If ssl is enabled, the application can be accessed over this port
* `HOST`: Host name of the container
* `SSL`: Flag which determines whether ssl needs to be enabled or not
* `CSP`: Flag for CSP
* `DEV`: Flag for Dev environment

#### Checking the docker logs.

```
docker-compose logs --tail="all"
```

'use strict'

var send = require('send')
var path = require('path')
var url = require('url')
var parseUrl = require('parseurl')
var argv = require('yargs')
    .default({dev: 1, portssl: 8444, port: 8080, hostname: 'localhost', csp: 1, ssl: 1})
    .argv

var fs = require('fs')

var winston = require('winston')

//
// Requiring `winston-loggly-bulk` will expose
// `winston.transports.Loggly`
//
require('winston-loggly-bulk')
let logTags = []
if (argv.dev) {
  logTags = ['dev-frontend-server']
} else {
  logTags = ['live-frontend-server']
}
const options = {
  bufferOptions: {
    size: 1000,
    retriesInMilliSeconds: 60 * 1000
  },
  level: 'info',
  json: true,
  token: '9a6800e4-1b03-4a9a-b190-6c5cb194a688',
  subdomain: 'playcoin',
    //
    // Optional: Tag to send with EVERY log message
    //
  tags: logTags

}
winston.add(winston.transports.Loggly, options)
winston.add(winston.transports.File, { level: 'silly', json: false, filename: 'access.log' })
winston.remove(winston.transports.Console)
let logger = winston

var responseFunc = function (req, res) {
  if (argv.csp) {
    res.setHeader('Content-Security-Policy', "default-src 'none'; script-src 'self'; connect-src 'self' " + argv.hostname + ':2053 wss://' + argv.hostname + ":2053; img-src 'self' " + argv.hostname + ":2053 shapeshift.io *.shapeshift.io *.gravatar.com; font-src 'self' https://fonts.gstatic.com https://fonts.googleapis.com data:; style-src 'self' https://fonts.googleapis.com 'unsafe-inline'")
  }
    // OPTIONS request, open CORS to all domains
  if (req.method === 'OPTIONS' && req.headers['access-control-request-method']) {
    if (req.headers['access-control-request-mothod'] !== 'GET') {
      res.writeHead(403)
      return res.end('invalid request method')
    }
    res.setHeader('access-control-allow-headers', req.headers['access-control-request-headers'])
    res.setHeader('access-control-allow-methods', 'GET')
    res.setHeader('access-control-allow-origin', '*')
    res.writeHead(204)
    return res.end()
  }
    // CORS for requests other than OPTIONS
  if (req.headers.origin) {
    res.setHeader('access-control-allow-origin', '*')
  }
  var parsedUrl = url.parse(req.url)
  var requestPath = parsedUrl.path
  if (!requestPath) {
    requestPath = '/'
  }
    // if we want to run this app in a subdirectory it can be done like this
    // requestPath = requestPath.replace('/madetoburst', '');
    // get the right directory for the locale
  var indexDir = path.resolve(__dirname, 'build')
  fs.readdir(indexDir, function (err) {
    if (err) {
      return (logger.error(err))
    }
    logger.silly('trying to get %s', path.resolve(indexDir, requestPath))
        // try to get the actual requested file/directory
    send(req, parseUrl(req).pathname, {root: indexDir}).on('error', function (resa, resb) {
            // console.log("0--------------------",resa,resb); // wtf is this? TODO
            // if we could not find it
      if ((/\.[a-z]{1,4}$/).test(req.url)) {
                // if it is a single file, return a 404
        res.statusCode = 404
        return res.end('404 file not found')
      } else {
                // if a "directory" was requested, serve the index
        requestPath = '/index.html'
      }
      logger.info('failed once, trying %s', path.resolve(indexDir, requestPath))
      send(req, parseUrl(req).pathname, {root: indexDir}).on('error', function (e, e2) {
        res.statusCode = e.statusCode
        logger.error(e.statusCode, e.code, e.path, path.resolve(indexDir, requestPath))
        res.end(e.code)
      }).pipe(res)
    }).pipe(res)
  })
}

logger.info('starting server with arguments:', argv)
// ssl is now optional
if (argv.ssl) {
  const tlsOptions = {
    key: fs.readFileSync('./ssl/key', 'utf8'),
    cert: fs.readFileSync('./ssl/cert', 'utf8'),
    port: argv.portssl,
    ciphers: 'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!SRP:!CAMELLIA'
  }

  var server = require('https').createServer(tlsOptions, responseFunc)
  server.listen(argv.portssl, function (err) {
    if (err) { throw err }
    logger.info('started https server on port %d', argv.portssl)
  })
}
// always start http server
var httpserver = require('http').createServer(function (req, res) {
  if (argv.ssl) {
    const redirectUrl = req.url || '/'
    res.setHeader('location', 'https://' + argv.hostname + ':' + argv.portssl + redirectUrl)
    res.statusCode = 301
    res.end()
  } else {
    responseFunc(req, res)
  }
})
httpserver.listen(argv.port, function (err) {
  if (err) {
    logger.error('error starting http server on port ' + argv.port)
  }
  logger.info('started http server on port ' + argv.port)
})

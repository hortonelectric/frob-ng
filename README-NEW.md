#### Project Details.

Dependencies:
  - Angular

#### Clone the project

```
git clone git@bitbucket.org:hortonelectric/frob-ng.git
```

#### Note on ENV variables
Please note that the application uses various environment variables for running as well as connecting with the API server. Please look into `.env` file for more details

#### Building Docker image.

Docker image can be built using docker-compose. Please read README-docker.md for more details on the docker setup

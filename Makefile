
GULP=./node_modules/.bin/gulp
API=localhost:2053
SERVER_PORT=80
SERVER_PORT_SSL=443
SERVER_PORT_DEV=8080
SERVER_PORT_SSL_DEV=8444
SERVER_HOSTNAME=localhost
SSL=1
SSL_DEV=0
CSP=1
CSP_DEV=0
LOCALE=en_US

test:
	npm run test

build: clean
	$(GULP) --locale $(LOCALE) --api $(API)

watch: build
	$(GULP) watch --locale $(LOCALE)

server:
	node server --dev 0 --port $(SERVER_PORT) --portssl $(SERVER_PORT_SSL) --hostname $(SERVER_HOSTNAME) --ssl $(SSL)

dev:
	node server --dev 1 --port $(SERVER_PORT_DEV) --portssl $(SERVER_PORT_SSL_DEV) --hostname $(SERVER_HOSTNAME) --ssl $(SSL_DEV) --csp $(CSP_DEV)

clean: test
	@rm -rf build

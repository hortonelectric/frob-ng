'use strict'

var fs = require('fs')
// gulp
var gulp = require('gulp')
var gutil = require('gulp-util')
var concat = require('gulp-concat-util')
var gulpFilter = require('gulp-filter')
var debug = require('gulp-debug')
// ejs
var ejs = require('gulp-ejs')
// lessCSS
var less = require('gulp-less')
// less plugin to minify resulting css
var LessPluginCleanCSS = require('less-plugin-clean-css')
var LessAutoprefix = require('less-plugin-autoprefix')
var cleancss = new LessPluginCleanCSS({ advanced: true })
var autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] })
// js minification
var uglify = require('gulp-uglify')
var sourcemaps = require('gulp-sourcemaps')
// directories
var SRC_DIR = 'src'
var LOCALE_DIR = 'locales'
var BUILD_DIR = 'build'

// get args
var argv = require('yargs')
    .default({locale: 'en_US'})
    .argv

// get locale files
function getTranslations (locale, cb) {
  fs.readFile('./' + LOCALE_DIR + '/' + locale + '.json', function (err, fileData) {
    if (err) {
      return cb(err)
    }
    try {
      var data = JSON.parse(fileData)
      data.locale = locale
      data.argv = argv
      return cb(null, data)
    } catch (ex) {
      return cb(ex)
    }
  })
}

// source paths
var paths = {
  scripts: [
    SRC_DIR + '/js/**/*.js',
    '!' + SRC_DIR + '/js/**/*flycheck*.js'
  ],
  vendor: [
    'bower_components/**/*'
  ],
  templates: [SRC_DIR + '/tpl/**/*.html'],
  bootstrap: [SRC_DIR + '/bootstrap/**/*.*'],
  fonts: [SRC_DIR + '/fonts/**/*.*'],
  directives: [SRC_DIR + '/directives/**/*.html'],
  index: [SRC_DIR + '/index.html'],
  res: [SRC_DIR + '/res/**/*'],
  images: [SRC_DIR + '/img/**/*'],
  assets: [SRC_DIR + '/assets/**/*'],
  less: [SRC_DIR + '/less/*.less']
}

var getBuildDest = function (path) {
  if (!path) {
    return BUILD_DIR
  }
  if (path[0] !== '/') {
    path = '/' + path
  }
  return gulp.dest(BUILD_DIR + path)
}

// HTML

gulp.task('index', function (cb) {
  getTranslations(argv.locale, function (err, ejsData) {
    if (err) {
      return cb(err)
    }
    return gulp.src(paths.index)
            .pipe(debug({title: 'index ejs op'}))
            .pipe(ejs(ejsData).on('error', gutil.log))
            .pipe(gulp.dest(getBuildDest()))
            .on('end', cb)
  })
})

gulp.task('templates', function (cb) {
  getTranslations(argv.locale, function (err, ejsData) {
    if (err) {
      return cb(err)
    }
    return gulp.src(paths.templates)
            .pipe(debug({title: 'template ejs op'}))
            .pipe(ejs(ejsData).on('error', gutil.log))
            .pipe(getBuildDest('tpl'))
            .on('end', cb)
  })
})

gulp.task('directives', function (cb) {
  getTranslations(argv.locale, function (err, ejsData) {
    if (err) {
      return cb(err)
    }
    return gulp.src(paths.directives)
            .pipe(debug({title: 'directive ejs op'}))
            .pipe(ejs(ejsData).on('error', gutil.log))
            .pipe(getBuildDest('directives'))
            .on('end', cb)
  })
})
// Res

gulp.task('res', function () {
  return gulp.src(paths.res)
        .pipe(getBuildDest('res'))
})
// Images

gulp.task('images', function () {
  return gulp.src(paths.images)
        .pipe(getBuildDest('img'))
})

// assets
gulp.task('assets', function () {
  return gulp.src(paths.assets)
        .pipe(getBuildDest('assets'))
})

// LESS

gulp.task('less', function () {
  return gulp.src(paths.less)
        .pipe(sourcemaps.init())
        .pipe(less({
          plugins: [autoprefix, cleancss]
        }))
        .pipe(sourcemaps.write())
        .pipe(getBuildDest('css'))
})

// JS

gulp.task('vendor', function () {
  return gulp.src(paths.vendor)
        .pipe(getBuildDest('vendor'))
})

gulp.task('bootstrap', function () {
  return gulp.src(paths.bootstrap)
        .pipe(getBuildDest('bootstrap'))
})
gulp.task('fonts', function () {
  return gulp.src(paths.fonts)
        .pipe(getBuildDest('fonts'))
})

gulp.task('packagejson', function () {
  return gulp.src(SRC_DIR + '/../package.json')
        .pipe(gulp.dest(getBuildDest()))
})

gulp.task('scripts', [], function (cb) {
  var apiFilter = gulpFilter('**/api.js', {restore: true})
  var configFilter = gulpFilter('src/config.js', {restore: true})
  getTranslations(argv.locale, function (err, ejsData) {
    if (err) {
      return cb(err)
    }
    return gulp.src(paths.scripts)
        // operate only on the api service file for ejs
            .pipe(apiFilter)
            .pipe(debug({title: 'api ejs op'}))
            .pipe(ejs({
              api: {
                host: (argv.api)
              },
              locale: argv.locale
            }).on('error', gutil.log))
        // now go back to operating on all of the js files
            .pipe(apiFilter.restore)
        // operate only on the messages service file for ejs
            .pipe(configFilter)

            .pipe(debug({title: 'messages ejs op'}))
            .pipe(ejs(ejsData).on('error', gutil.log))
        // now go back to operating on all of the js files
            .pipe(configFilter.restore)
            .pipe(sourcemaps.init())
            .pipe(uglify())
            .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()) })
            .pipe(sourcemaps.write())
            .pipe(concat.scripts('app.min.js'), {newLine: ';'})
            .pipe(getBuildDest('js'))
            .on('end', cb)
  })
})

gulp.task('watch', function () {
  for (var task in paths) {
    if (paths.hasOwnProperty(task)) {
      gulp.watch(paths[task], [task])
    }
  }
  gulp.watch(LOCALE_DIR + '/*.json', ['index', 'templates', 'scripts'])
  gulp.watch(SRC_DIR + '/js/services/routes.js', ['index', 'templates'])
})

gulp.task('default', [
  'index',
  'templates',
  'directives',
  'bootstrap',
  'fonts',
  'packagejson',
  'less',
  'vendor',
  'scripts',
  'res',
  'assets',
  'images'
])
